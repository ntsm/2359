<?php
use PHPUnit\Framework\TestCase;

class RabbitTest extends TestCase
{
    function testConnection()
    {
        $connection = \App\Base\Rabbit::connection('test','test','test');
        $this->assertTrue($connection->isConnected(), 'Rabbit connection failed');
    }
}