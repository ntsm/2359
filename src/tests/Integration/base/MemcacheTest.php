<?php
use PHPUnit\Framework\TestCase;

class MemcacheTest extends TestCase
{
    function testConnection()
    {
        $connection = \App\Base\Memcache::instance();
        $this->assertNotEmpty($connection->getServerList(),'Memcache connection failed');
    }
}