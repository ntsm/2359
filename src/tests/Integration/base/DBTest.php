<?php
use PHPUnit\Framework\TestCase;

class DBTest extends TestCase
{
    function testConnection()
    {
        $connection = \App\Base\DB::connection();
        $a = $connection->query('select 1');
        $res = $a->fetch();
        $this->assertSame("1", $res[1], 'Db connection failed');
    }
}