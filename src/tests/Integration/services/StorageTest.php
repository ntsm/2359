<?php

use PHPUnit\Framework\TestCase;
use App\Services\Storage as StorageService;
use App\Base\DB;

class Storage extends TestCase
{

    const TABLE = 'TEST_heap';
    /** @var \App\Services\Storage */
    private $__storage;
    private $__DB;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->__prepare();
    }
    /************************************************************************************/
    /**
     * @dataProvider mPushCorrectProvider
     */
    public function testMpush_correct_returnCount($data)
    {
        //A
        $this->__truncate();
        $expected = count($data);
        //A
        $this->__storage->mPush($data);
        //A
        $actual = count($this->__storage->mGet(1, time(), false));
        $this->assertSame($expected, $actual);

    }

    public function mPushCorrectProvider()
    {
        $time = time() - 10;
        return [
            [
                [
                    [
                        StorageService::FIELD_TIMESTAMP => $time,
                        StorageService::FIELD_ENDPOINT => 'mPushCorrectProvider1',
                        StorageService::FIELD_DATA => 'test1',
                        StorageService::FIELD_QUEUE_ID => 1
                    ]
                ]
            ],
            [
                [
                    [
                        StorageService::FIELD_TIMESTAMP => $time,
                        StorageService::FIELD_ENDPOINT => 'mPushCorrectProvider2',
                        StorageService::FIELD_DATA => rand(1, 100),
                        StorageService::FIELD_QUEUE_ID => 1
                    ],
                    [
                        StorageService::FIELD_TIMESTAMP => $time,
                        StorageService::FIELD_ENDPOINT => 'mPushCorrectProvider2',
                        StorageService::FIELD_DATA => rand(1, 100),
                        StorageService::FIELD_QUEUE_ID => 1
                    ],
                    [
                        StorageService::FIELD_TIMESTAMP => $time,
                        StorageService::FIELD_ENDPOINT => 'mPushCorrectProvider2',
                        StorageService::FIELD_DATA => rand(1, 100),
                        StorageService::FIELD_QUEUE_ID => 1
                    ],
                    [
                        StorageService::FIELD_TIMESTAMP => $time,
                        StorageService::FIELD_ENDPOINT => 'mPushCorrectProvider2',
                        StorageService::FIELD_DATA => rand(1, 100),
                        StorageService::FIELD_QUEUE_ID => 1
                    ],
                    [
                        StorageService::FIELD_TIMESTAMP => $time,
                        StorageService::FIELD_ENDPOINT => 'mPushCorrectProvider2',
                        StorageService::FIELD_DATA => rand(1, 100),
                        StorageService::FIELD_QUEUE_ID => 1
                    ],
                ]
            ],
        ];
    }
    /************************************************************************************/
    /**
     * @dataProvider mPushInCorrectProvider
     */
    public function testMpush_incorrect_returnCount($data)
    {
        //A
        $this->__truncate();
        //A
        $this->__storage->mPush($data);
        //A
        $actual = count($this->__storage->mGet(1, time(), false));
        $this->assertSame(0, $actual);
    }

    public function mPushInCorrectProvider()
    {
        $time = time() - 10;
        return [
            [
                []
            ],
            [
                [
                    [
                        StorageService::FIELD_ENDPOINT => 'mPushInCorrectProvider1',
                        StorageService::FIELD_DATA => 'test1',
                        StorageService::FIELD_QUEUE_ID => 1
                    ]
                ]
            ],
            [
                [
                    [
                        StorageService::FIELD_TIMESTAMP => $time,
                        StorageService::FIELD_ENDPOINT => 'mPushInCorrectProvider2',
                        StorageService::FIELD_DATA => 'test1',
                        StorageService::FIELD_QUEUE_ID => 0
                    ]
                ]
            ],
            [
                [
                    [
                        StorageService::FIELD_TIMESTAMP => $time,
                        StorageService::FIELD_ENDPOINT => 'mPushInCorrectProvider3',
                        StorageService::FIELD_DATA => 'test1',
                        StorageService::FIELD_QUEUE_ID => getenv('PUBLISHER_QUEUEDS') + 1
                    ]
                ]
            ],
        ];
    }
    /************************************************************************************/
    /**
     * @dataProvider mPush2queuedProvider
     */
    public function testMpush_2queued($data)
    {
        //A
        $this->__truncate();
        //A
        $this->__storage->mPush($data);
        //A
        $actual = count($this->__storage->mGet(1, time(), false));
        $this->assertSame(1, $actual);
        $actual = count($this->__storage->mGet(2, time(), false));
        $this->assertSame(1, $actual);
    }

    public function mPush2queuedProvider()
    {
        $time = time() - 10;
        return [
            [
                [
                    [
                        StorageService::FIELD_TIMESTAMP => $time,
                        StorageService::FIELD_ENDPOINT => 'mPush2queued1',
                        StorageService::FIELD_DATA => 'test1',
                        StorageService::FIELD_QUEUE_ID => 1
                    ],
                    [
                        StorageService::FIELD_TIMESTAMP => $time,
                        StorageService::FIELD_ENDPOINT => 'mPush2queued1',
                        StorageService::FIELD_DATA => 'test1',
                        StorageService::FIELD_QUEUE_ID => 2
                    ],
                    [
                        StorageService::FIELD_TIMESTAMP => $time,
                        StorageService::FIELD_DATA => 'test1',
                        StorageService::FIELD_QUEUE_ID => 1
                    ],
                ]
            ],
        ];
    }
    /************************************************************************************/
//TODO RELEASE TIMES
//    /**
//     * @dataProvider releaseTimesProvider
//     */
//    public function testReleaseTimes($time, $data)
//    {
//        //A
//        $this->__truncate();
//        //A
//        $this->__storage->mPush($data);
//        //A
//        $release = $this->__storage->getReleaseTimes();
//        $this->assertSame($time - 20, $release[$this->__storage->getReleaseKeyName() . '1']);
//        $this->assertSame($time, $release[$this->__storage->getReleaseKeyName() . '2']);
//    }
//
//    public function releaseTimesProvider()
//    {
//        $time = time();
//        return [
//            [
//                $time,
//                [
//                    [
//                        StorageService::FIELD_TIMESTAMP => $time + 10,
//                        StorageService::FIELD_ENDPOINT => 'mPush2queued1',
//                        StorageService::FIELD_DATA => 'test1',
//                        StorageService::FIELD_QUEUE_ID => 1
//                    ],
//                    [
//                        StorageService::FIELD_TIMESTAMP => $time - 20,
//                        StorageService::FIELD_ENDPOINT => 'mPush2queued1',
//                        StorageService::FIELD_DATA => 'test1',
//                        StorageService::FIELD_QUEUE_ID => 1
//                    ],
//                    [
//                        StorageService::FIELD_TIMESTAMP => $time,
//                        StorageService::FIELD_ENDPOINT => 'mPush2queued2',
//                        StorageService::FIELD_DATA => 'test1',
//                        StorageService::FIELD_QUEUE_ID => 2
//                    ],
//                    [
//                        StorageService::FIELD_TIMESTAMP => $time - 100,
//                        StorageService::FIELD_DATA => 'test1',
//                        StorageService::FIELD_QUEUE_ID => 1
//                    ],
//                ]
//            ],
//        ];
//    }
    /************************************************************************************/

    /**
     * @dataProvider mPop1queuedProvider
     */
    public function testmPop_1queued($time, $expected, $data)
    {
        //A
        $this->__truncate();
        //A
        $this->__storage->mPush($data);
        //A
        $data = $this->__storage->mPop(1, $time + 3);

        $this->assertSame($expected, count($data));
        $data = $this->__storage->mGet(1, $time + 3, false);
        $this->assertSame(0, count($data));
        $data = $this->__storage->mGet(1, $time + 3, true);
        $this->assertSame($expected, count($data));
    }

    public function mPop1queuedProvider()
    {
        $time = time();
        return [
            [
                $time, 2,
                [
                    [
                        StorageService::FIELD_TIMESTAMP => $time + 2,
                        StorageService::FIELD_ENDPOINT => 'testmPop1',
                        StorageService::FIELD_DATA => 'test1',
                        StorageService::FIELD_QUEUE_ID => 1
                    ],
                    [
                        StorageService::FIELD_TIMESTAMP => $time - 10,
                        StorageService::FIELD_ENDPOINT => 'testmPop1',
                        StorageService::FIELD_DATA => 'test1',
                        StorageService::FIELD_QUEUE_ID => 1
                    ],
                ]
            ],
        ];
    }
    /************************************************************************************/
    /**
     * @dataProvider mPop2queuedProvider
     */
    public function testmPop_2queued($time, $expected, $data)
    {
        //A
        $this->__truncate();
        //A
        $this->__storage->mPush($data);
        //A
        $data = $this->__storage->mPop(2, $time + 3);
        $this->assertSame([], $data);
        $data = $this->__storage->mGet(2, $time + 10, false);
        $this->assertSame($expected, count($data));
        $data = $this->__storage->mGet(2, $time + 3, true);
        $this->assertSame(0, count($data));
    }

    public function mPop2queuedProvider()
    {
        $time = time();
        return [
            [
                $time, 1,
                [
                    [
                        StorageService::FIELD_TIMESTAMP => $time + 2,
                        StorageService::FIELD_ENDPOINT => 'testmPop1',
                        StorageService::FIELD_DATA => 'test1',
                        StorageService::FIELD_QUEUE_ID => 1
                    ],
                    [
                        StorageService::FIELD_TIMESTAMP => $time + 10,
                        StorageService::FIELD_ENDPOINT => 'testmPop1',
                        StorageService::FIELD_DATA => 'test1',
                        StorageService::FIELD_QUEUE_ID => 2
                    ],
                ]
            ],
        ];
    }

    /************************************************************************************/

    private function __prepare()
    {
        $this->__DB = DB::connection();
        $sql = '
                CREATE TABLE `' . self::TABLE . '` (
                  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                  `timestamp` bigint(20) unsigned NOT NULL,
                  `queue_id` int(10) unsigned NOT NULL,
                  `endpoint` varchar(255) NOT NULL,
                  `data` text NOT NULL,
                  `deleted` tinyint(4) DEFAULT NULL,
                  PRIMARY KEY (`id`),
                  KEY `queue_id_timestamp_deleted` (`queue_id`,`timestamp`,`deleted`)
                ) ENGINE=InnoDB AUTO_INCREMENT=609 DEFAULT CHARSET=utf8
        ';
        $this->__DB->exec($sql);

        $this->__storage = new StorageService(self::TABLE);
    }

    private function __truncate()
    {
//TODO RELEASE TIME
//        foreach (range(1, getenv('PUBLISHER_QUEUEDS')) as $id) {
//            $this->__storage->deleteReleaseTime($id);
//        }
        $sql = 'truncate ' . self::TABLE;
        $this->__DB->exec($sql);
    }

    public function __destruct()
    {
        $sql = 'drop table if exists '.self::TABLE;
        $this->__DB->exec($sql);
    }
}