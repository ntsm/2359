<?php
require_once __DIR__ . '/../vendor/autoload.php';
new \App\Base\Config(__DIR__ . '/.env');

use App\Base\Rabbit;

$q = 'asd123_'.$argv[1];
$ex = '2359p';
$connection = Rabbit::connection($q, $ex, 99);
$channel = $connection->channel();
$channel->queue_declare($q, false, true, false, true);
$channel->exchange_declare($ex, \PhpAmqpLib\Exchange\AMQPExchangeType::DIRECT, false, true, true);
$channel->queue_bind($q, $ex, $q);
$j = 0;
for ($i=1;$i<1000;$i++) {
    $msg = $channel->basic_get($q);
    if(!empty($msg)) {
        $j++;
        var_dump($msg->body);
        $channel->basic_ack($msg->delivery_info['delivery_tag']);
    }
}
var_dump($j);