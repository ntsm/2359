<?php

namespace App\Base;

use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Processor\HostnameProcessor;
use Monolog\Processor\IntrospectionProcessor;
use Monolog\Processor\MemoryPeakUsageProcessor;
use Monolog\Processor\ProcessIdProcessor;
use Monolog\Processor\UidProcessor;

class Logger extends \Monolog\Logger
{
    const DEFAULT_CHANNEL = 'w2359';
    private static $instance;

    public function __construct($level = self::ERROR, $name = null)
    {
        $this->name = ($name !== null) ? $name : self::DEFAULT_CHANNEL;
        parent::__construct(
            $this->name,
            [
                new RotatingFileHandler($this->getFilePath(), 30, $level),
                new FirePHPHandler()
            ],
            [
                new IntrospectionProcessor(),
                new MemoryPeakUsageProcessor(),
                new ProcessIdProcessor(),
                new UidProcessor(),
                new HostnameProcessor(),
            ]
        );
    }

    private function getFilePath()
    {
        return __DIR__ . getenv('LOG_PATH').'/' . $this->name.'.log';
    }
    /**
     * @param null $level
     * @param null $name
     * @param null $info
     * @return self
     */
    public static function instance($level = null, $name = null)
    {
        if (!isset(self::$instance[$name])) {
            self::$instance[$name] = new self($level, $name);
        }
        return self::$instance[$name];
    }
}