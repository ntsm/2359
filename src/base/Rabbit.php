<?php

namespace App\Base;

use PhpAmqpLib\Connection\AMQPSSLConnection;

class Rabbit
{

    const VHOST = '/';
    const READ_WRITE_TIMEOUT = 30;
    const KEEP_ALIVE = true;
    const HEARTBEAT = 15;

    private static $instance = [];

    private $connection;
    private $exchange;
    private $queue;
    private $consumerTag;

    public function __construct($queue, $exchange, $consumerId)
    {
        $this->connection = new AMQPSSLConnection(
            getenv('RABBIT_HOST'),
            getenv('RABBIT_PORT'),
            getenv('RABBIT_USER'),
            getenv('RABBIT_PASS'),
            self::VHOST,
            null,
            [
                'read_write_timeout' => self::READ_WRITE_TIMEOUT,    // needs to be at least 2x heartbeat
                'keepalive' => self::KEEP_ALIVE, // doesn't work with ssl connections
                'heartbeat' => self::HEARTBEAT
            ]
        );

        $this->queue = $queue;
        $this->exchange = $exchange;
        $this->consumerTag = $consumerId;
    }


    /**
     * @param $queue
     * @param $exchange
     * @param $consumerId
     * @return mixed|AMQPSSLConnection
     */
    public static function connection($queue, $exchange, $consumerId)
    {
        if (!isset(self::$instance[$consumerId])) {
            self::$instance[$consumerId] = (new self($queue, $exchange, $consumerId))->connection;
        }
        if(!self::$instance[$consumerId]->isConnected()) {
            self::$instance[$consumerId]->reconnect();
        }
        return self::$instance[$consumerId];
    }


}