<?php
namespace App\Base;

class Memcache extends \Memcached
{
    private static $instance;

    private $host;
    private $port;

    public function __construct($persistent_id = '', $on_new_object_cb = null)
    {
        parent::__construct($persistent_id = '', $on_new_object_cb = null);

        $this->host = getenv('MEMCACHE_HOST');
        $this->port = getenv('MEMCACHE_PORT');
        $this->addServer($this->host, $this->port);
    }

    /**
     * @return Memcache
     */
    public static function instance()
    {
        if(empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }



}