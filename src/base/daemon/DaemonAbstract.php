<?php

namespace App\Base\Daemon;

use Psr\Log\LoggerInterface;

abstract class DaemonAbstract implements DaemonInterface
{
    private $restart = false;
    private $stop = false;

    private $logger;

    /**
     * DaemonAbstract constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        if (extension_loaded('pcntl')) {
            pcntl_signal(SIGTERM, [$this, 'signalHandler']);
            pcntl_signal(SIGHUP, [$this, 'signalHandler']);
            pcntl_signal(SIGINT, [$this, 'signalHandler']);
            pcntl_signal(SIGQUIT, [$this, 'signalHandler']);
            pcntl_signal(SIGUSR1, [$this, 'signalHandler']);
            pcntl_signal(SIGUSR2, [$this, 'signalHandler']);
            pcntl_signal(SIGALRM, [$this, 'alarmHandler']);
        } else {
            $this->logger->critical('UNABLE TO PROCESS SIGNALS');
            exit(1);
        }
    }

    final private function dispatch()
    {
        pcntl_signal_dispatch();
    }

    /**
     * @param int $signalNumber
     */
    public function signalHandler($signalNumber)
    {
        $this->logger->debug('SIGNAL: #' . $signalNumber);

        switch ($signalNumber) {
            case SIGTERM:  // 15 : supervisor default stop
            case SIGQUIT:  // 3  : kill -s QUIT
                $this->stopHard();
                break;
            case SIGINT:   // 2  : ctrl+c
                $this->stop();
                break;
            case SIGHUP:   // 1  : kill -s HUP
                $this->restart();
                break;
            case SIGUSR1:  // 10 : kill -s USR
                // send an alarm in 1 second
                pcntl_alarm(1);
                break;
            case SIGUSR2:  // 12 : kill -s USR2
                // send an alarm in 10 seconds
                pcntl_alarm(10);
                break;
            default:
                break;
        }
    }

    /**
     * @param int $signalNumber
     */
    public function alarmHandler($signalNumber)
    {
        $this->logger->alert('ALARM: #' . $signalNumber);
    }

    public function restart()
    {
        $this->logger->debug('RESTART');
        $this->restart = true;
    }

    public function stopHard()
    {
        $this->logger->critical('STOPPING HARD');
        $this->stop = true;
    }

    public function stopSoft()
    {
        $this->logger->warning('STOPPING SOFT');
    }

    public function stop()
    {
        $this->logger->info('STOPPING');
        $this->restart = true;
        $this->stop = true;
    }

    public function shouldRestart()
    {
        return !$this->restart;
    }
    public function shouldStop()
    {
        return !$this->stop;
    }

    final public function lifeCircle()
    {
        do{
            do {
                $this->prepare();
                $this->work();
                $this->flush();
                $this->dispatch();
            } while ($this->shouldRestart());
        } while ($this->shouldStop());
    }
}
