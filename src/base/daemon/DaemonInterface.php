<?php

namespace App\Base\Daemon;

interface DaemonInterface
{
    /**
     * Signal handler
     *
     * @param int $signalNumber
     * @return void
     */
    public function signalHandler($signalNumber);
    /**
     * Alarm handler
     *
     * @param int $signalNumber
     * @return void
     */
    public function alarmHandler($signalNumber);
    /**
     * @param $function
     * @throws \ErrorException
     */
    /**
     * Restart the consumer on an existing connection
     */
    public function restart();
    /**
     * Close the connection to the server
     */
    public function stopHard();
    /**
     * Close the channel to the server
     */
    public function stopSoft();
    /**
     * Tell the server you are going to stop consuming
     * It will finish up the last message and not send you any more
     */
    public function stop();
    public function work();
    public function prepare();
    public function flush();


    public function shouldRestart();
    public function shouldStop();
}