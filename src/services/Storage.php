<?php

namespace App\Services;

use App\Base\DB;
use App\Base\Logger;
use App\Base\Memcache;
use Psr\Log\LoggerInterface;

class Storage
{
    const DEFAULT_TABLE = 'heap';

    const FIELD_ID = 'id';
    const FIELD_TIMESTAMP = 'timestamp';
    const FIELD_QUEUE_ID = 'queue_id';
    const FIELD_ENDPOINT = 'endpoint';
    const FIELD_DATA = 'data';
    const FIELD_DELETED = 'deleted';

    const M_KEY_RELEASE_TIME_SUFFIX = 'frt.';

    const LIMIT = 100;

    private $table;
    private $dbConn;
    private $cache;
    private $releaseTimes;
    /** @var LoggerInterface $logger */
    private $logger;

    public function __construct($table = self::DEFAULT_TABLE)
    {
        $this->dbConn = Db::connection();
        $this->cache = Memcache::instance();
//TODO RELEASE TIMES
//        $this->releaseTimes = $this->getReleaseTimes();
        $this->table = $table;
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param $level
     * @param $message
     * @param array $context
     */
    private function log($level, $message, $context = [])
    {
        if(!empty($this->logger)) {
            $this->logger->log($level, $message, $context = []);
        }
        return;
    }
    /**
     * @param array $data
     * @return bool|false|int
     */
    public function mPush(array $data)
    {
        if (empty($data)) {
            return false;
        }

        $tmp = '';
//TODO RELEASE TIMES
//        $saveReleaseTime = false;
//        $this->releaseTimes = $this->getReleaseTimes();
        foreach ($data as $row) {
            if (!$this->isSafeRow($row)) {
                continue;
            }

            $tmp .= '(' .
                $row[self::FIELD_TIMESTAMP] . ',' .
                $this->dbConn->quote($row[self::FIELD_ENDPOINT]) . ',' .
                $row[self::FIELD_QUEUE_ID] . ',' .
                $this->dbConn->quote($row[self::FIELD_DATA]) .
            '),';
//TODO RELEASE TIMES
//            if ($this->isNewReleaseTime($row[self::FIELD_TIMESTAMP], $row[self::FIELD_QUEUE_ID])) {
//                var_dump('new realese time --- '.$row[self::FIELD_QUEUE_ID].' '.$row[self::FIELD_TIMESTAMP]);
//                $saveReleaseTime = true;
//                $this->releaseTimes[$this->getReleaseKeyName() . $row[self::FIELD_QUEUE_ID]] = $row[self::FIELD_TIMESTAMP];
//            }
        }

        if ($tmp === '') {
            return false;
        }
//TODO RELEASE TIMES
//        if ($saveReleaseTime) {
//            $this->setReleaseTimes($this->releaseTimes);
//        }

        $tmp[strlen($tmp) - 1] = ';';

        $sql = sprintf(
            'INSERT INTO %s (%s,%s,%s,%s) VALUES %s',
            $this->table, self::FIELD_TIMESTAMP, self::FIELD_ENDPOINT, self::FIELD_QUEUE_ID, self::FIELD_DATA,
            $tmp
        );
        return $this->dbConn->exec($sql);
    }

    /**
     * @param $data
     * @return bool
     */
    private function isSafeRow($data)
    {
        if (
            isset($data[self::FIELD_TIMESTAMP]) &&
            isset($data[self::FIELD_ENDPOINT]) &&
            isset($data[self::FIELD_QUEUE_ID]) &&
            isset($data[self::FIELD_DATA]) &&
            $data[self::FIELD_QUEUE_ID] > 0 && $data[self::FIELD_QUEUE_ID] <= getenv('PUBLISHER_QUEUEDS')
        ) {
            return true;
        }
        $this->log(Logger::WARNING, 'NOT SAFE DATA', $data);

        return false;
    }

    /**
     * @param int $queueId
     * @param int $timestamp
     * @param bool $deleted
     * @param int $limit
     * @return array|bool
     */
    public function mGet(int $queueId, int $timestamp, bool $deleted, $limit = self::LIMIT)
    {
        if (empty($queueId) && empty($timestamp)) {
            return false;
        }
        $deleted = $deleted ? 'IS NOT NULL' : 'IS NULL';
        $sql = sprintf(
            'SELECT %s, %s, %s, %s, %s FROM %s 
                    WHERE %s=%d AND %s <= %d AND %s %s  
                    LIMIT %d;',
            self::FIELD_ID, self::FIELD_QUEUE_ID, self::FIELD_TIMESTAMP, self::FIELD_DATA, self::FIELD_ENDPOINT, $this->table,
            self::FIELD_QUEUE_ID, $queueId, self::FIELD_TIMESTAMP, $timestamp, self::FIELD_DELETED, $deleted,
            $limit
        );
        $statement = $this->dbConn->query($sql);
        return $statement ? $statement->fetchAll() : false;
    }

    /**
     * @param int $queueId
     * @param int $timestamp
     * @param int $limit
     * @return array|bool
     */
    public function mPop(int $queueId, int $timestamp, $limit = self::LIMIT)
    {
        if (empty($queueId) || empty($timestamp)) {
            return false;
        }

        $rows = $this->mGet($queueId, $timestamp, false, $limit);
        $ids = ($rows) ? array_column($rows, self::FIELD_ID) : [];
        if (empty($ids)) {
            return [];
        }

        $sql = sprintf(
            'UPDATE %s SET %s = 1 WHERE %s IN(%s)',
            $this->table, self::FIELD_DELETED, self::FIELD_ID, implode(',', $ids)
        );

        $statement = $this->dbConn->query($sql);

        return ($statement !== false) ? $rows : false;
    }

//TODO RELEASE TIMES
//    public function getReleaseTimeById($id)
//    {
//        return $this->cache->get($this->getReleaseKeyName() . $id);
//    }
//TODO RELEASE TIMES
//    public function getReleaseTimes()
//    {
//        return $this->cache->getMulti($this->getReleaseTimeKeys());
//    }
//TODO RELEASE TIMES
//    /**
//     * @param $time
//     * @param $queue
//     * @return bool
//     */
//    public function isReleaseTime($time, $queue)
//    {
//        $this->releaseTimes = $this->getReleaseTimes();
//        if (
//            isset($this->releaseTimes[$this->getReleaseKeyName() . $queue]) &&
//            $time > $this->releaseTimes[$this->getReleaseKeyName() . $queue]) {
//            return true;
//        }
//        return false;
//    }
//TODO RELEASE TIMES
//    /**
//     * @param $queue
//     * @return bool
//     */
//    public function deleteReleaseTime($queue)
//    {
//        unset($this->releaseTimes[$this->getReleaseKeyName() . $queue]);
//        $this->cache->delete($this->getReleaseKeyName() . $queue);
//        return true;
//    }

//TODO RELEASE TIMES
//    public function getReleaseKeyName()
//    {
//        if ($this->table == self::DEFAULT_TABLE) {
//            return self::M_KEY_RELEASE_TIME_SUFFIX;
//        } else {
//            //TODO IT's BAD WAY
//            return 'test_' . self::M_KEY_RELEASE_TIME_SUFFIX;
//        }
//    }
//TODO RELEASE TIMES
//    /**
//     * @return array
//     */
//    private function getReleaseTimeKeys()
//    {
//        $ret = [];
//        foreach (range(1, getenv('PUBLISHER_QUEUEDS')) as $id) {
//            $ret[] = $this->getReleaseKeyName() . $id;
//        }
//        return $ret;
//    }
//TODO RELEASE TIMES
//    /**
//     * To cache
//     * @param array $data
//     * @return bool
//     */
//    private function setReleaseTimes(array $data)
//    {
//        return $this->cache->setMulti($data);
//    }
//TODO RELEASE TIMES
//    /**
//     * @param $time
//     * @param $queue
//     * @return bool
//     */
//    private function isNewReleaseTime($time, $queue)
//    {
//        if (
//            !isset($this->releaseTimes[$this->getReleaseKeyName() . $queue]) ||
//            $time < $this->releaseTimes[$this->getReleaseKeyName() . $queue]) {
//            return true;
//        }
//        return false;
//    }
}