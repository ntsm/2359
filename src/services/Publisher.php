<?

namespace App\Services;

use App\Base\Daemon\DaemonAbstract;
use App\Base\Rabbit;
use PhpAmqpLib\Connection\AMQPSSLConnection;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

class Publisher extends DaemonAbstract
{
    const CHUNK = 100;
    const TICK_STEP_SECONDS = 1;

    private $id;

    private $logger;
    private $lastTickStartTime;
    private $isTick;

    private $storage;
    /** @var AMQPSSLConnection[] */
    private $connections = [];

    public function __construct($id, LoggerInterface $logger)
    {
        parent::__construct($logger);
        $this->logger = $logger;
        $this->isTick = false;
        $this->id = $id;

        $this->storage = new Storage();
    }

    public function work()
    {
        if ($this->isTick) {
            $time = time();
            $this->logger->info($this->lastTickStartTime);
            if (1/*$this->storage->isReleaseTime($time, $this->id)*/) {//TODO RELEASE TIMES
                $data = $this->storage->mPop($this->id, $time, self::CHUNK);
//                $this->logger->debug('RELEASE_TIME', ['id' => $this->id, 'time' => $time, 'msgCount' => count($data)]);
                if ($data) {
                    $tmp = [];
                    foreach ($data as $raw) {
                        $tmp[$raw[Storage::FIELD_ENDPOINT]][] = $raw[Storage::FIELD_DATA];
                    }
                    foreach ($tmp as $endpoint => $rows) {
                        $conn = $this->getConnection($endpoint, getenv('RABBIT_PUBLISHER_DEFAULT_EXCHANGE'));

                        $conn->channel($this->id)->queue_declare($endpoint, false, true, false, true);
                        $conn->channel($this->id)->exchange_declare(getenv('RABBIT_PUBLISHER_DEFAULT_EXCHANGE'), AMQPExchangeType::DIRECT, false, true, true);
                        $conn->channel($this->id)->queue_bind(
                            $endpoint,
                            getenv('RABBIT_PUBLISHER_DEFAULT_EXCHANGE'),
                            $endpoint
                        );

                        foreach ($rows as $row) {
                            $conn->channel($this->id)->basic_publish(
                                $this->getMsg($row),
                                getenv('RABBIT_PUBLISHER_DEFAULT_EXCHANGE'),
                                $endpoint
                            );
                            $this->logger->debug('=============\nPUBLISH MSG TO ['.$endpoint.']', json_decode($row,true));
                        }
                    }
                }
//TODO RELEASE TIMES
//                if (empty($data)) {
//                    $this->storage->deleteReleaseTime($this->id);
//                }
            }
        }
    }

    public function prepare()
    {
        if ($this->isTick) {
            $this->lastTickStartTime = microtime(true);
        }
    }

    public function flush()
    {
        $this->isTick = $this->checkTick();

        if ($this->isTick) {
            if (!empty($this->connections)) {
                foreach ($this->connections as $conn) {
                    $conn->close();
                }
            }
            $this->connections = [];
        }
    }

    private function checkTick()
    {
        return (microtime(true) - $this->lastTickStartTime) >= self::TICK_STEP_SECONDS;
    }

    /**
     * @param $messageBody
     * @return AMQPMessage
     */
    private function getMsg($messageBody)
    {
        return new AMQPMessage(
            json_encode($messageBody),
            array(
                'content_type' => 'application/json',
                'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
            )
        );
    }

    /**
     * @param $queue
     * @param $exchange
     * @return mixed|AMQPSSLConnection
     */
    private function getConnection($queue, $exchange)
    {
        if (!isset($this->connections[$this->id])) {
            $this->connections[$this->id] = Rabbit::connection($queue, $exchange, $this->id);
        }
        return $this->connections[$this->id];
    }

    /**
     * @deprecated
     * @param $queue
     * @param $exchange
     * @return string
     */
    private function getConnectionId($queue, $exchange)
    {
        return $queue . '.' . $exchange;
    }
}