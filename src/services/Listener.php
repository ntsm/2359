<?php


namespace App\Services;

use App\Base\Daemon\DaemonAbstract;
use App\Base\Memcache;
use App\Base\Rabbit;

use PhpAmqpLib\Exchange\AMQPExchangeType;
use Psr\Log\LoggerInterface;


class Listener extends DaemonAbstract
{

    const CHUNK_SIZE = 100;
    const EMPTY_DATA_SLEEP_SECONDS = 1;
    public $connection;
    public $channel;


    private $queue;
    private $exchange;
    private $consumerTag;

    private $storage;
    private $cache;
    private $logger;

    private $queueCount;

    public function __construct($queue, $exchange, $consumerId, LoggerInterface $logger)
    {
        parent::__construct($logger);

        $this->queue = $queue;
        $this->exchange = $exchange;
        $this->consumerTag = $consumerId;

        $this->connection = Rabbit::connection($queue, $exchange, $consumerId);
        $this->channel = $this->connection->channel($this->consumerTag);
        $this->channel->queue_declare($this->queue, false, true, false, true);
        $this->channel->exchange_declare($this->exchange, AMQPExchangeType::DIRECT, false, true, true);
        $this->channel->queue_bind($this->queue, $this->exchange);

        $this->storage = new Storage();
        $this->storage->setLogger($logger);
        $this->logger = $logger;

        $this->cache = Memcache::instance();

        $this->queueCount = getenv('PUBLISHER_QUEUEDS');

    }

    private function getEndpointQueueId($endpoint)
    {
        return (crc32($endpoint) % $this->queueCount) + 1;
    }

    private function formatMsg(array $data, $queue)
    {
        return [
            Storage::FIELD_TIMESTAMP => $data[2],
            Storage::FIELD_ENDPOINT => $data[0],
            Storage::FIELD_DATA => $data[1],
            Storage::FIELD_QUEUE_ID => $queue
        ];
    }

    /**
     * @param $data
     * @param $queue
     * @return bool
     */
    private function validateMsg($data)
    {
        if (
            !is_array($data) ||
            !isset($data[0]) || empty($data[0]) ||
            !isset($data[1]) || empty($data[1]) ||
            !isset($data[2]) || empty($data[2])
        ) {
            $this->logger->warning('BAD MSG DATA', ['data' => $data, 'queue' => $data[0]]);
            return false;
        }
        return true;
    }

    public function work()
    {
        $deliveryTag = false;
        $toSave = [];

        for ($i = 0; $i < self::CHUNK_SIZE; $i++) {
            $msg = $this->channel->basic_get($this->queue);
            if ($msg !== NULL) {
                $tmp = json_decode($msg->body, true);

                if (!$this->validateMsg($tmp)) {
                    continue;
                }

                $queue = $this->getEndpointQueueId($tmp[0]);
                $toSave[] = $this->formatMsg($tmp, $queue);

                $deliveryTag = $msg->delivery_info['delivery_tag'];
            } else {
                //TODO RELEASE TIMES
                //TODO WORK WITH RELEASE TIMES REPLACE TO NEW CLASS
                //TODO REFACTORING MAIN LOGIC IN RELEASE TIMES
                sleep(self::EMPTY_DATA_SLEEP_SECONDS);
                break;
            }
        }

        if ($this->storage->mPush($toSave)) {
            $this->logger->debug('SAVED TO STORAGE', $toSave);

            $this->channel->basic_ack($deliveryTag, true);
            $this->logger->debug('LAST TAG', ['tag' => $deliveryTag]);
        };

    }

    /**
     * Close the connection to the server
     */
    public function stopHard()
    {
        parent::stopHard();
        $this->connection->close();
    }

    /**
     * Close the channel to the server
     */
    public function stopSoft()
    {
        parent::stopSoft();
        $this->channel->close();
    }

    /**
     * Tell the server you are going to stop consuming
     * It will finish up the last message and not send you any more
     */
    public function stop()
    {
        parent::stop();
        $this->channel->basic_cancel($this->consumerTag, false, true);
    }

    public function prepare()
    {
        return;
    }

    public function flush()
    {
        return;
    }


}