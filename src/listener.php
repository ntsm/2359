<?php
require_once __DIR__ . '/../vendor/autoload.php';
new \App\Base\Config(__DIR__ . '/.env');


/**
 * Queue name for listen
 * @var $queue
 */
$queue = !empty($argv[1]) ? $argv[1] : getenv('LISTENER_DEFAULT_QUEUED_NAME');
/** @var  $exchange */
$exchange = !empty($argv[2]) ? $argv[2] : getenv('RABBIT_LISTENER_DEFAULT_EXCHANGE');
/**
 * Sharding ID
 * @var  $consumerId
 */
$consumerId = !empty($argv[3]) ? $argv[3] : 1;

$debugLevel = (!empty($argv[4])) ? \App\Base\Logger::DEBUG : \App\Base\Logger::NOTICE;


/**
 * Error log file
 * @var  $errorLog
 */
$errorLog = \App\Base\Logger::instance($debugLevel, 'w2359.listener.' . $queue .'_'.$consumerId);

/** Declare ErrorHandler for File Log */
\Monolog\ErrorHandler::register($errorLog);

/**
 * Declare worker
 * @var  $daemon
 */
$daemon = new \App\Services\Listener($queue, $exchange, $consumerId, $errorLog);
/** Start worker */
$daemon->lifeCircle();


register_shutdown_function('shutdown', $daemon->channel, $daemon->connection, $errorLog);

function shutdown($channel, $connection, \App\Base\Logger $errorLog)
{
    //TODO
    $errorLog->emergency('shutdown', (!empty(error_get_last())) ? error_get_last() : []);
    $channel->close();
    $connection->close();
}


