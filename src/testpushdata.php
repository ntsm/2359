<?php
require_once __DIR__ . '/../vendor/autoload.php';
new \App\Base\Config(__DIR__ . '/.env');

use App\Base\Rabbit;
use PhpAmqpLib\Message\AMQPMessage;

$exch = '2359l';
$queued = '2359';
$connection = Rabbit::connection($queued, $exch, 123);
$channel = $connection->channel(123);
$channel->queue_declare($queued, false, true, false, true);
$channel->exchange_declare($exch, \PhpAmqpLib\Exchange\AMQPExchangeType::DIRECT, false, true, true);
$channel->queue_bind($queued, $exch);

$data = [];
for ($i=0;$i<1000;$i++) {
    $r = rand(1,2);
    $data[] = [
        'asd123_'.$r,
        json_encode(['somedata' => rand(1,100), 'r' => $r]),
        time()+rand(1,300)
    ];
}

foreach ($data as $messageBody) {
    $connection
        ->channel()
        ->basic_publish(
            new AMQPMessage(
                json_encode($messageBody),
                array(
                    'content_type' => 'application/json',
                    'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
                )
            ),
            $exch
        );
}
