<?php
require_once __DIR__ . '/../vendor/autoload.php';
new \App\Base\Config(__DIR__ . '/.env');

/**
 * Sharding ID
 * @var  $consumerId
 */
$consumerId = !empty($argv[1]) ? $argv[1] : 1;

$debugLevel = (!empty($argv[2])) ? \App\Base\Logger::DEBUG : \App\Base\Logger::NOTICE;

$errorLog = \App\Base\Logger::instance($debugLevel, 'w2359.publisher_error.' . $consumerId);

\Monolog\ErrorHandler::register($errorLog);

$daemon = new \App\Services\Publisher($consumerId,
    \App\Base\Logger::instance($debugLevel, 'w2359.publisher_debug.' . $consumerId)
);

$daemon->lifeCircle();
