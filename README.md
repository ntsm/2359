# LISTENER
## Start script:

`php listener.php [queueName] [exchangePath] [consumerId] [debug]`
 
`php listener.php 2359 2359 1 debug`

## Параметры

`queueName` - название очереди в которое пушатся сообщения для обработки. При необходимости можно шардировать. По умолчанию - `2359`
 
`exchangePath` - точка обмена. В логике в данный момент никак не используется, все по мануалам кролика. По умолчанию - `2359`

`consumerId` - id консумера. По умолчанию - `1`

`debug` - если параметр есть, в логи пишется дебаг информация.

# PUBLISHER 
## Start script:

`php publisher.php [queueId] [debug]`

## Параметры

`queueId` - Id очереди
 
`debug` - если параметр есть, в логи пишется дебаг информация.

  
# TESTS
Run tests:

`php tests.php tests/`

